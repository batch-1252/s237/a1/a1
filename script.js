// 4false 2 admin
db.users.insertMany(
    [
    {
        "firstName":"Diane",
        "lastName":"Murphy",
        "Email":"dmurphy@mail.com",
        "IsAdmin":false,
        "IsActive":true
    },
    {
        "firstName":"Mary",
        "lastName":"Patterson",
        "Email":"mpatterson@mail.com",
        "IsAdmin":false,
        "IsActive":true
    },
    {
        "firstName":"Jeff",
        "lastName":"Firrelli",
        "Email":"jfirrelli@mail.com",
        "IsAdmin":false,
        "IsActive":true
    },
    {
        "firstName":"Gerard",
        "lastName":"Bondur",
        "Email":"gbondur@mail.com",
        "IsAdmin":false,
        "IsActive":true
    },
    {
        "firstName":"Pamela",
        "lastName":"Castillo",
        "Email":"gcastillo@mail.com",
        "IsAdmin":true,
        "IsActive":false
    },
    {
        "firstName":"George",
        "lastName":"Vanauf",
        "Email":"gvanauf@mail.com",
        "IsAdmin":true,
        "IsActive":false
    }
    ]);

db.couse.insertMany(
    [{
        "courseName":"Professional Development",
        "Price":"10000"
    },
    {
        "courseName":"Business Processing",
        "Price":"13000"
    }]
    )

db.users.updateOne(
    {"courseName": "Professional Development"},
    {
        $set: { "Enrolled":  "Murphy, Firrelli"},
    }
    );
db.users.updateOne(
    {"courseName": "Business Processing"},
    {
        $set: { "Enrolled":  "Bondur, Patterson"},
    }
    );

db.users.find({"IsAdmin":false});